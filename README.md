# TensorFlow Lab
This repo contains a lab using react and tensorflow to  demonstrate in browser machine learning. This particular lab uses MNSIT data to create an application that can predict hand written numbers.

## Getting Started

To get started, simply run:

* `npm i`
* `npm start`