import React, { Component } from 'react';
import Trainer from '../../components/trainer';
import {Subscribe} from 'react-contextual';

export default class Training extends Component {
    render() {
        return (
            <div className='padding'>
                <Subscribe>
                    {store => {
                        return (
                            <Trainer model={store.model}
                                     loaded={store.loaded}
                                     training={store.training}
                                     setTraining={store.setTraining}/>
                        );
                    }}
                </Subscribe>
            </div>
        );
    }
};