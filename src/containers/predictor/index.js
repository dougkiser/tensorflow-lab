import React, { Component } from 'react';
import Predictor from '../../components/predictor';
import {Subscribe} from 'react-contextual';

export default class Predicting extends Component {
    render() {
        return (
            <div className='padding'>
                <Subscribe>
                    {store => {
                        return (
                            <Predictor model={store.model}/>
                        );
                    }}
                </Subscribe>
            </div>
        );
    }
};