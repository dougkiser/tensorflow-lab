import React from 'react';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';
import {Provider} from 'react-contextual';
import Training from './containers/training';
import Predictor from './containers/predictor';
import AppHeader from './components/app-header';
import store from './store';

export default () => (
  <div className='app'>
      <Provider {...store}>
          <Router>
               <div>
                  <AppHeader/>
                  <Route exact path='/' component={Training}/>
                  <Route path='/training' component={Training}/>
                  <Route path='/predictor' component={Predictor}/>
              </div>
           </Router>
      </Provider>
  </div>
);