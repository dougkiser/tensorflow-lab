import * as tf from '@tensorflow/tfjs';

const LEARNING_RATE = 0.3;

const addMaxPoolingToModel = (model) => {
    return model.add(tf.layers.maxPooling2d({
        poolSize: [2, 2],
        strides: [2, 2]
    }));
};

const addConvolutionLayerToModel = (model, options) => {
    return model.add(tf.layers.conv2d(options));
};

const addFlattenedLayerToModel = (model) => {
    model.add(tf.layers.flatten());
};

const addDenseLayerToModel = (model, options) => {
    model.add(tf.layers.dense(options));
};

export default () => {
    const model = tf.sequential();
    const optimizer = tf.train.sgd(LEARNING_RATE);
    
    addConvolutionLayerToModel(model, {
        inputShape: [28, 28, 1], // 28x28 pixel input
        kernelSize: 5,
        filters: 8,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'varianceScaling'
    });
    addMaxPoolingToModel(model);
    addConvolutionLayerToModel(model, {
        kernelSize: 5,
        filters: 16,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'varianceScaling'
    });
    addMaxPoolingToModel(model);
    addFlattenedLayerToModel(model);
    addDenseLayerToModel(model, {
        units: 10, // number of output classes
        kernelInitializer: 'varianceScaling',
        activation: 'softmax' // create probability distribution over classes so that the output sum is 1
    });
    model.compile({
        optimizer,
        loss: 'categoricalCrossentropy',
        metrics: ['accuracy']
    });
    return model;
};