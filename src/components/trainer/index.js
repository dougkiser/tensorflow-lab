import React, {
    Component
} from 'react';
import {
    Button,
    Typography,
    CircularProgress,
    Paper
} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import {
    LineChart,
    Line,
    YAxis,
    ResponsiveContainer
} from 'recharts';
import {
    MnistData
} from '../../data';
import {
    BATCH_SIZE,
    TRAIN_BATCHES,
    TEST_BATCH_SIZE,
    TEST_ITERATION_FREQUENCY
} from './trainer.constants';
import './trainer.css';

const styles = theme => ({
    root: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,
        marginTop: theme.spacing.unit * 3,
    }),
    buttonContainer: {
        display: 'flex'
    },
    buttonWrapper: {
        margin: theme.spacing.unit,
        position: 'relative'
    },
    buttonProgress: {
        position: 'absolute',
        marginLeft: '-3.5rem',
        marginTop: '0.25rem'
    },
    flexSpacer: {
        flex: 1
    },
    resultContainer: {
        display: 'flex',
        flexWrap: 'wrap'
    }
});

class Trainer extends Component {
    state = {
        loaded: true,
        training: false,
        predicting: false,
        data: [{loss: 0, accuracy: 100}]
    };
    
    componentDidMount () {
        this._loadData();
    }
    
    render () {
        const {
            training,
            loaded,
            classes
        } = this.props;
        const {batch} = this.state;
        
        return (
            <div>
                <Typography variant='headline' component='h2'>
                    Trainer
                </Typography>
                <p>
                    This is the trainer. Here you can train our model to predict handwritten single digits using the MNIST data set. During training you will see the graph plot loss and accuracy in real time.
                    You can train as many times as you choose but as the model grows, so does the time it takes to complete training. After you complete a training cycle you can predict against a set of a 100
                    generated images of digits from the MNIST data set by pressing the Predict button to see how you are doing.
                </p>
                <Paper elevation={4} className={classes.root}>
                    <ResponsiveContainer width='100%' height={500}>
                        <LineChart data={this.state.data}>
                            <YAxis/>
                            <Line type="monotone" stroke='#F44336' dataKey='loss' dot={false}/>
                            <Line type="monotone" stroke='#1DE9B6' dataKey='accuracy' dot={false}/>
                        </LineChart>
                    </ResponsiveContainer>
                    {loaded && (
                        <div className={classes.buttonContainer}>
                            <div className={classes.flexSpacer}/>
                            <div className={classes.buttonWrapper}>
                                <Button color='primary'
                                        variant='contained'
                                        disabled={training}
                                        onClick={this._train}>
                                    Train
                                </Button>
                                {training && (
                                    <CircularProgress size={24} className={classes.buttonProgress} color='secondary'/>
                                )}
                            </div>
                            <div className={classes.buttonWrapper}>
                                <Button disabled={training}
                                        variant='contained'
                                        color='secondary'
                                        onClick={this._predict}>
                                    Predict
                                </Button>
                            </div>
                        </div>
                    )}
                    {batch && (<div id='image-container' className={classes.resultContainer}/>)}
                </Paper>
            </div>
        );
    }
    
    _generateResults (testExamples) {
        const imageContainer = document.getElementById('image-container');
        const listContainer = document.createElement('div');
        const ul = document.createElement('ul');
        const accuracyLabel = document.createElement('div');
        
        listContainer.className = 'prediction-result-container';
        ul.style = 'list-style-type: none;';
        ul.className = 'prediction-list';
        let count = 0;
        Array.from(Array(testExamples)).forEach((ignore, index) => {
            const image = this.state.batch.xs.slice([index, 0], [1, this.state.batch.xs.shape[1]]);
            const li = document.createElement('li');
            const canvas = document.createElement('canvas');
            const pred = document.createElement('div');
            const prediction = this.state.predictions[index];
            const label = this.state.labels[index];
            const correct = prediction === label;
            if (correct) {
                count++;
            }
            
            this._draw(image.flatten(), canvas);
            li.className = 'prediction-container';
            canvas.className = 'prediction-canvas';
            pred.className = `prediction ${(correct ? 'prediction-correct' : 'prediction-incorrect')}`;
            pred.innerText = prediction;
            li.appendChild(pred);
            li.appendChild(canvas);
            ul.appendChild(li);
            listContainer.appendChild(ul);
        });
        accuracyLabel.className = 'accuracy-label';
        accuracyLabel.innerText = count + '% Correct';
        listContainer.insertBefore(accuracyLabel, listContainer.childNodes[0]);
        imageContainer.appendChild(listContainer);
    }
    
    _draw (image, canvas) {
        const [width, height] = [28, 28];
        const ctx = canvas.getContext('2d');
        const imageData = new ImageData(width, height);
        const data = image.dataSync();
        
        canvas.width = width;
        canvas.height = height;
        
        Array.from(Array(height * width)).forEach((ignore, index) => {
            const j = index * 4;
            imageData.data[j] = data[index] * 255;
            imageData.data[j + 1] = data[index] * 255;
            imageData.data[j + 2] = data[index] * 255;
            imageData.data[j + 3] = 255;
        });
        
        ctx.putImageData(imageData, 0, 0);
    }
    
    _predict = () => {
        this.setState(() => ({
            predicting: true
        }), () => {
            const testExamples = 100;
            const batch = this.data.nextTestBatch(testExamples);
            
            window.tf.tidy(() => {
                const output = this.props.model.predict(batch.xs.reshape([-1, 28, 28, 1]));
                const axis = 1;
                const labels = Array.from(batch.labels.argMax(axis).dataSync());
                const predictions = Array.from(output.argMax(axis).dataSync());
                
                this.setState(() => ({
                    predictions,
                    labels,
                    batch,
                    predicting: false
                }), () => this._generateResults(testExamples));
            });
        })
    }
    
    _getBatch (index) {
        return window.tf.tidy(() => {
            const batch = this.data.nextTrainBatch(BATCH_SIZE);
            batch.xs = batch.xs.reshape([BATCH_SIZE, 28, 28, 1]);
            
            let validationData;
            // Every few batches test the accuracy of the model.
            if (index % TEST_ITERATION_FREQUENCY === 0) {
                const testBatch = this.data.nextTestBatch(TEST_BATCH_SIZE);
                validationData = [
                    // Reshape the training data from [64, 28x28] to [64, 28, 28, 1] so
                    // that we can feed it to our convolutional neural net.
                    testBatch.xs.reshape([TEST_BATCH_SIZE, 28, 28, 1]), testBatch.labels
                ];
            }
            return [batch, validationData];
        });
    }
    
    async _trainBatch ({batch, validationData}) {
        // The entire data set doesn't fit into memory so we call train repeatedly
        // with batches using the fit() method.
        return this.props.model.fit(
            batch.xs,
            batch.labels, {
                batchSize: BATCH_SIZE,
                validationData,
                epochs: 1
            }
        );
    }
    
    _train = async () => {
        this.props.setTraining(true);
        
        const lossValues = [];
        const accuracyValues = [];
        
        console.time('training cycle took');
        for (let i = 0; i < TRAIN_BATCHES; i++) {
            // Iteratively train our model on mini-batches of data.
            const [batch, validationData] = this._getBatch(i)
            
            const history = await this._trainBatch({
                batch,
                validationData
            });
            
            const loss = history.history.loss[0];
            const accuracy = history.history.acc[0];
            
            // Plot loss / accuracy.
            lossValues.push({
                'batch': i,
                'loss': loss,
                'set': 'train'
            });
            
            if (validationData != null) {
                accuracyValues.push({
                    'batch': i,
                    'accuracy': accuracy,
                    'set': 'train'
                });
            }
            
            // Call dispose on the training/test tensors to free their GPU memory.
            window.tf.dispose([batch, validationData]);
            
            // tf.nextFrame() returns a promise that resolves at the next call to
            // requestAnimationFrame(). By awaiting this promise we keep our model
            // training from blocking the main UI thread and freezing the browser.
            await window.tf.nextFrame();
            const mappedLossValues = [...lossValues.map((value, index) => {
                return ({
                    loss: value.loss * 100,
                    accuracy: accuracyValues[index] ? accuracyValues[index].accuracy * 100 : null
                });
            })];
            this.setState(() => ({
                data: mappedLossValues
            }));
        }
        console.timeEnd('training cycle took');
        
        this.props.setTraining(false);
    };
    
    _loadData = () => {
        this.data = new MnistData();
        this.data.load().then(() => this.setState(() => ({
            loaded: true
        })));
    }
}

export default withStyles(styles)(Trainer);