export const BATCH_SIZE = 64;
export const TRAIN_BATCHES = 100;
export const TEST_BATCH_SIZE = 1000;
export const TEST_ITERATION_FREQUENCY = 5;