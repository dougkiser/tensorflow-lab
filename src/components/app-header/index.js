import React from 'react';
import {AppBar, Typography, Toolbar, Button} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import {
    Link
} from 'react-router-dom';

const styles = {
    root: {
        flexGrow: 1
    },
    flex: {
        flex: 1
    },
    headerLink: {
        textDecoration: 'none'
    }
};

const AppHeader = props => {
    const {classes} = props;
    return (
        <AppBar position='static' color='default'>
            <Toolbar>
                <Typography variant="title" color="inherit" className={classes.flex}>
                    TensorFlow Lab
                </Typography>
                <Button color='inherit'>
                    <Link to="/training" className={classes.headerLink}>Training</Link>
                </Button>
                <Button color='inherit'>
                    <Link to="/predictor" className={classes.headerLink}>Predictor</Link>
                </Button>
            </Toolbar>
        </AppBar>
    );
};

export default withStyles(styles)(AppHeader);