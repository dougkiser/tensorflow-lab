import React, {
    Component
} from 'react';
import {
    Button,
    Typography,
    Paper
} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';

const INPUT_PIXEL_SIZE = 28;
const BRUSH_COLOR = '#fff';
const BRUSH_DIAMETER = 30;
const CANVAS_SIZE = 300;

const drawLine = ({ canvas, startPosition, endPosition, brushColor, brushDiameter }) => {
    const ctx = canvas.getContext('2d');
    ctx.save();
    ctx.strokeStyle = brushColor;
    ctx.lineWidth = brushDiameter;
    ctx.lineCap = 'round';
    ctx.beginPath();
    ctx.moveTo(startPosition.x, startPosition.y);
    ctx.lineTo(endPosition.x + 1, endPosition.y + 1);
    ctx.stroke();
    ctx.restore();
};

const getImageDataAndScale = ({ canvas, outputSize }) => {
    const {width, height} = outputSize;
    const scaled = document.createElement('canvas');
    const scaledCtx = scaled.getContext('2d');
    scaled.width = width;
    scaled.height = height;

    scaledCtx.drawImage(canvas, 0, 0, width, height);
    document.getElementById('sample').appendChild(scaled);
    return scaledCtx.getImageData(0, 0, width, height);
};

const styles = theme => ({
    root: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,
        marginTop: theme.spacing.unit * 3,
        width: '300px'
    }),
    flex: {
      display: 'flex'
    },
    flexColumn: {
        flex: 1,
        width: '50%'
    },
    sample: {
        minHeight: '32px'
    },
    prediction: {
        flex: 1,
        listStyleType: 'none',
        marginLeft: '0.575rem',
        marginRight: '0.575rem'
    },
    predictions: {
        display: 'inline-flex',
        paddingLeft: 0
    },
    buttonContainer: {
        display: 'flex'
    },
    buttonWrapper: {
        margin: theme.spacing.unit,
        position: 'relative'
    },
    flexSpacer: {
        flex: 1
    }
});

class Predictor extends Component {
    state = {
        predictions: []
    };

    componentDidMount() {
        this._initializeCanvas();
        this.mousePosition = {};
    }

    render() {
        const {classes} = this.props;
        return (
            <div>
                <Typography variant='headline' component='h2'>
                    Predictor
                </Typography>
                <p>
                    This is the predictor. Here will we use our trained model to predict handwritten digits. To get started, draw a single digit on the canvas and
                    click the Predict button to see the result. If the predictions are not very accurate, try training the model a little more and try again.
                </p>
                <Paper elevation={4} className={classes.root}>
                    <div className={classes.flex}>
                        <div className={classes.flexColumn}>
                            <div>
                                <canvas className='drawing-board'
                                        onMouseMove={this._onMouseMove}
                                        onMouseDown={this._onMouseDown}
                                        ref={el => (this.drawingBoard = el)} />
                            </div>
                            <div className={classes.buttonContainer}>
                                <div className={classes.flexSpacer}/>
                                <div className={classes.buttonWrapper}>
                                    <Button color='primary'
                                            variant='contained'
                                            onClick={this._makePrediction}>Predict</Button>
                                </div>
                                <div className={classes.buttonWrapper}>
                                    <Button color='secondary'
                                            variant='contained'
                                            onClick={this._clearDrawingBoard}>Clear</Button>
                                </div>
                            </div>
                            <div>
                                {!!this.state.predictions.length && (
                        
                                    <ul className={classes.predictions}>
                                        {this.state.predictions.map((prediction, index) => (
                                            <li className={classes.prediction} key={index}>{prediction}</li>
                                        ))}
                                    </ul>
                    
                                )}
                                <div id='sample' className={classes.sample} ref={el => (this.sample = el)} />
                            </div>
                        </div>
                        
                    </div>
                </Paper>
            </div>
        );
    }

    _initializeCanvas() {
        this.drawingBoard.width = CANVAS_SIZE;
        this.drawingBoard.height = CANVAS_SIZE;
        const context = this.drawingBoard.getContext('2d');
        context.fillStyle = 'black';
        context.fillRect(0, 0, CANVAS_SIZE, CANVAS_SIZE);
    }

    _onMouseDown = event => {
        this.isDrawing = true;
        this._updateMousePosition({
            x: event.clientX,
            y: event.clientY
        });
        this._draw();
        window.addEventListener('mouseup', this._onMouseUp);
    };

    _onMouseUp = event => {
        this.isDrawing = false;
        window.removeEventListener('mouseup', this._onMouseUp);
    };

    _onMouseMove = event => {
        this._updateMousePosition({
            x: event.clientX,
            y: event.clientY
        });

        if (this.isDrawing) {
            this._draw();
        }

        event.preventDefault();
    };

    _updateMousePosition = ({x, y}) => {
        const {left, top} = this.drawingBoard.getBoundingClientRect();
        this.mousePosition.x = x - left;
        this.mousePosition.y = y - top;
    };

    _draw() {
        drawLine({
            canvas: this.drawingBoard,
            startPosition: this.mousePosition,
            endPosition: this.mousePosition,
            brushColor: BRUSH_COLOR,
            brushDiameter: BRUSH_DIAMETER
        });
    }

    _clearDrawingBoard = () => {
        const ctx = this.drawingBoard.getContext('2d');
        ctx.clearRect(0, 0, this.drawingBoard.width, this.drawingBoard.height);
        ctx.fillStyle = 'black';
        ctx.fillRect(0, 0, CANVAS_SIZE, CANVAS_SIZE);
    };

    _makePrediction = () => {
        const imageData = getImageDataAndScale({
            canvas: this.drawingBoard,
            outputSize: {
                width: INPUT_PIXEL_SIZE,
                height: INPUT_PIXEL_SIZE
            }
        });
        this._predict(imageData);
    };

    async _predict(imageData) {
        const {model} = this.props;
        await window.tf.tidy(() => {
            const tensorImage = window.tf
                .fromPixels(imageData, 1)
                .reshape([1, INPUT_PIXEL_SIZE, INPUT_PIXEL_SIZE, 1]);
            const imageToPredict = window.tf.cast(tensorImage, 'float32').div(window.tf.scalar(255));
            const prediction = model.predict(imageToPredict);
            const predictionData = Array.from(prediction.dataSync());

            this.setState(() => ({
                predictions: [...this.state.predictions, predictionData.indexOf(Math.max(...predictionData))]
            }));
        });
    }
}

export default withStyles(styles)(Predictor);