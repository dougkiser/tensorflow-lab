const LEARNING_RATE = 0.3;

/*
This layer will downsample the result(also known as the activation) from the convolution by computing the maximum value
for each sliding window
*/
const addMaxPoolingToModel = (model) => {
    return model.add(window.tf.layers.maxPooling2d({
        poolSize: [2, 2], // size of the sliding pooling window to be applied to the input data.
        strides: [2, 2] // the step size of the sliding pooling window
    }));
};

/*
This method adds a 2d convolutional layer to our model. Convolutions slide a filter window over an image to learn transformations
that are spatially invariant or it will treat patterns or objects in different parts of the image the same way.
*/
const addConvolutionLayerToModel = (model, options) => {
    return model.add(window.tf.layers.conv2d(options));
};

/*
This method will add a flatten layer to flatten the output of the previous layer to a vector
*/
const addFlattenedLayerToModel = (model) => {
    model.add(window.tf.layers.flatten());
};

/*
This method will add a dense layer(also known as a fully connected layer), which will perform the final classfication.
*/
const addDenseLayerToModel = (model, options) => {
    model.add(window.tf.layers.dense(options));
};

/*
This method will compile the given model with an optimizer, loss function, and a list of evaluation metrics
*/
const compileModel = (model) => {
    const optimizer = window.tf.train.sgd(LEARNING_RATE);
    model.compile({
        optimizer,
        loss: 'categoricalCrossentropy',
        metrics: ['accuracy']
    });
};

export default () => {
    const model = window.tf.sequential(); // this type of model will pass the input sequentially from one layer to the next
    
    addConvolutionLayerToModel(model, {
        inputShape: [28, 28, 1], // 28x28 pixel input
        kernelSize: 5,
        filters: 8,
        strides: 1,
        activation: 'relu', // the activation is the function to apply the data after the convolution is complete. Here it is Rectified Linear Unit (ReLU)
        kernelInitializer: 'varianceScaling' // The method used for randomly initializing the model weights, which is very important to training dynamics
    });
    addMaxPoolingToModel(model);
    addConvolutionLayerToModel(model, {
        kernelSize: 5,
        filters: 16,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'varianceScaling'
    });
    addMaxPoolingToModel(model);
    addFlattenedLayerToModel(model);
    addDenseLayerToModel(model, {
        units: 10, // number of output classes
        kernelInitializer: 'varianceScaling',
        activation: 'softmax' // create probability distribution over classes so that the output sum is 1
    });
    compileModel(model);
    return model;
};