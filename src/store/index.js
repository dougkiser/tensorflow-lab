import generateModel from './model';

const store = {
    model: generateModel(),
    loaded: true,
    training: false,
    setModel: model => () => ({
        model,
        loaded: true
    }),
    setTraining: training => () => ({
        training
    })
};

export default store;